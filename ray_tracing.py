from __future__ import division
import pylab as pl
from scipy.ndimage.interpolation import zoom, map_coordinates
from scipy.interpolate import RectBivariateSpline

#Given a lensmap and an image, performs the ray tracing
#INPUT: lm - lensing map object
#       img_map - background image object
#       pix_scale_bg - scaling for background pixels
#       rescale - optional keyword argument to rescale image
#OUTPUT: lensed - the lensed image
def ray_trace(lm, img_map, pix_scale_bg,  **kwargs):
    if 'rescale' in kwargs:
        rescale=kwargs['rescale']
    else:
        rescale=1
        
    nx=(int)(pl.shape(lm.map)[0]*rescale)
    nb=pl.shape(img_map.map)[0]
    

    b1_proj=zoom(lm.x-lm.alpha1, [nx/pl.shape(lm.x)[0], nx/pl.shape(lm.x)[1]])/pix_scale_bg+nb/2
    b2_proj=zoom(lm.y-lm.alpha2, [nx/pl.shape(lm.y)[0], nx/pl.shape(lm.y)[1]])/pix_scale_bg+nb/2


    #print b1_proj[0, :], b2_proj[:, 0]
    #spl=RectBivariateSpline(pl.arange(pl.shape(img_map.map)[0]), pl.arange(pl.shape(img_map.map)[1]), img_map.map)

    lensed_map=255-map_coordinates(img_map.map, [ b2_proj, b1_proj], mode='nearest')

    #lensed_map=spl.ev(b1_proj[0, :], b2_proj[:, 0])

    
    return lensed_map
